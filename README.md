# Week 5 miniproject
ATM Service: Allows users to register, then deposit, withdraw or check their balance

## Build Process

Before setting up the lambda, I first set up the RDS DB in the AWS console. The config looks like this
![alt text](image-1.png)

I shelled into my RDS instance by creating an EC2 with permissions to access the RDS service and then ran the script `sql/db_init.sql` in this repo using the `psql` command. This set up the table needed for my lambda to work.

I then built the lambda by first using
```
cargo lambda new
```

After filling out the `function_handler` function, I built the lambda using
```
cargo lambda build
```

and then deployed it to AWS using
```
cargo lambda deploy
```

Because I didn't want any secrets in my codebase (since this is a public GitHub repo), I configured the environment variables from the AWS console to pass in the RDS endpoint, username and password.

## Usage

Run a command like
```
curl -H "Content-Type: application/json" --data '{"name": "<insert name>", "pass": "<insert password>", "action": "register"}' https://cnutu5xlbi.execute-api.us-west-2.amazonaws.com/default/ns380-atm-lambda
```
This should register the user with the database. You can then interact with the service like
```
curl -H "Content-Type: application/json" --data '{"name": "<insert name>", "pass": "<insert password>", "action": "deposit", "amount": 100}' https://cnutu5xlbi.execute-api.us-west-2.amazonaws.com/default/ns380-atm-lambda
```
To deposit money into the account,

```
curl -H "Content-Type: application/json" --data '{"name": "<insert name>", "pass": "<insert password>", "action": "withdraw", "amount": 10}' https://cnutu5xlbi.execute-api.us-west-2.amazonaws.com/default/ns380-atm-lambda
```
To withdraw money from the account, and

```
curl -H "Content-Type: application/json" --data '{"name": "<insert name>", "pass": "<insert password>", "action": "balance"}' https://cnutu5xlbi.execute-api.us-west-2.amazonaws.com/default/ns380-atm-lambda
```
To check the balance of the account. If you pass the incorrect name-password combination, the service should prevent you from modifying the balance.

Here is a screenshot of the usage
![alt text](image.png)
