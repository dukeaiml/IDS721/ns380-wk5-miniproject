use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use serde::{Deserialize, Serialize};
use serde_json::from_str;
use tokio::spawn;
use tokio_postgres::{Config, NoTls};
use std::env;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RequestBody {
    pub name: String,
    pub pass: String,
    pub action: String,
    pub amount: Option<i32>,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract some useful information from the request
    let body = event.body();
    let s = std::str::from_utf8(body).expect("invalid utf-8 sequence");
    // Specify the endpoint information for the RDS instance
    let host = env::var("ENDPOINT").expect("ENDPOINT environment variable not set");
    let port = 5432; // Default PostgreSQL port for RDS
    let user = env::var("USERNAME").expect("USERNAME environment variable not set");
    let password = env::var("PASSWORD").expect("PASSWORD environment variable not set");
    let dbname = env::var("DBNAME").expect("DBNAME environment variable not set");

    // Connect to the PostgreSQL database using tokio-postgres
    let mut config = Config::new();
    config.host(host.as_str());
    config.port(port);
    config.user(user.as_str());
    config.password(password.as_str());
    config.dbname(dbname.as_str());

    let (client, conn) = config.connect(NoTls).await?;

    // The connection object performs the actual communication with the database,
    // so spawn it off to run on its own.
    spawn(async move {
        if let Err(e) = conn.await {
            eprintln!("connection error: {}", e);
        }
    });

    //Serialze JSON into struct.
    //If JSON is incorrect, send back 400 with error.
    let item = match from_str::<RequestBody>(s) {
        Ok(item) => item,
        Err(err) => {
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(("Body not provided correctly: ".to_string() + &err.to_string() + "\n").into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    let query_resp = client.query("SELECT COUNT(*) FROM users WHERE name = $1 AND pass = $2", &[&item.name, &item.pass]).await?;
    let user_cnt: i64 = query_resp[0].get(0);
    let has_creds: bool = user_cnt > 0;

    // Dispatch through match to handlers
    let _ = match item.action.to_lowercase().as_str() {
        "balance" => {
            if has_creds {
                let row = client.query("SELECT balance FROM users WHERE name = $1", &[&item.name]).await?;
                let balance: i32 = row[0].get(0);
                let resp = Response::builder()
                    .status(400)
                    .header("content-type", "text/html")
                    .body(("User ".to_string() + &item.name.to_string() + " has balance " + &balance.to_string() + "\n").into())
                    .map_err(Box::new)?;
                return Ok(resp);
            }
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(("Incorrect username or password\n".to_string()).into())
                .map_err(Box::new)?;
            return Ok(resp);
        },
        "register" => {
            let row = client.query("SELECT COUNT(*) FROM users WHERE name = $1", &[&item.name]).await?;
            let cnt: i64 = row[0].get(0);
            if cnt > 0 {
                let resp = Response::builder()
                    .status(200)
                    .header("content-type", "text/plain")
                    .body(("User already exists with name ".to_string() + &item.name.to_string() + "\n").into())
                    .map_err(Box::new)?;
                return Ok(resp); 
            }
            client.execute("INSERT INTO users (name, pass) VALUES ($1,$2)", &[&item.name, &item.pass]).await?;
            let resp = Response::builder()
                .status(200)
                .header("content-type", "text/plain")
                .body(("Successfully inserted user ".to_string() + &item.name.to_string() + "\n").into())
                .map_err(Box::new)?;
            return Ok(resp); 
        },
        "deposit" => {
            if item.amount.is_none() {
                let resp = Response::builder()
                    .status(400)
                    .header("content-type", "text/html")
                    .body(("Please provide an amount to deposit\n".to_string()).into())
                    .map_err(Box::new)?;
                return Ok(resp);
            }
            if item.amount.unwrap() < 0 {
                let resp = Response::builder()
                    .status(400)
                    .header("content-type", "text/html")
                    .body(("Provided amount cannot be negative\n".to_string()).into())
                    .map_err(Box::new)?;
                return Ok(resp);
            }
            if has_creds {
                client.execute("UPDATE users SET balance = balance + $1 WHERE name = $2", &[&item.amount.unwrap(), &item.name]).await?;
                let resp = Response::builder()
                    .status(200)
                    .header("content-type", "text/plain")
                    .body(("Successfully updated balance for user ".to_string() + &item.name.to_string() + "\n").into())
                    .map_err(Box::new)?;
                return Ok(resp); 
            }
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(("Incorrect username or password\n".to_string()).into())
                .map_err(Box::new)?;
            return Ok(resp);
        },
        "withdraw" => {
            if item.amount.is_none() {
                let resp = Response::builder()
                    .status(400)
                    .header("content-type", "text/html")
                    .body(("Please provide an amount to withdraw\n".to_string()).into())
                    .map_err(Box::new)?;
                return Ok(resp);
            }
            if item.amount.unwrap() < 0 {
                let resp = Response::builder()
                    .status(400)
                    .header("content-type", "text/html")
                    .body(("Provided amount cannot be negative\n".to_string()).into())
                    .map_err(Box::new)?;
                return Ok(resp);
            }
            if has_creds {
                client.execute("UPDATE users SET balance = balance - $1 WHERE name = $2", &[&item.amount.unwrap(), &item.name]).await?;
                let resp = Response::builder()
                    .status(200)
                    .header("content-type", "text/plain")
                    .body(("Successfully updated balance for user ".to_string() + &item.name.to_string() + "\n").into())
                    .map_err(Box::new)?;
                return Ok(resp); 
            }
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(("Incorrect username or password\n".to_string()).into())
                .map_err(Box::new)?;
            return Ok(resp);
        },
        _ => {
            let resp = Response::builder()
                .status(200)
                .header("content-type", "text/plain")
                .body(("An unsupported action was given. Supported actions are register, withdraw, deposit and balance. Provided action: ".to_string() + &item.action.to_string()).into())
                .map_err(Box::new)?;
            return Ok(resp);        
        }
    };
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
